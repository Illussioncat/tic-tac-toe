const API_URL = "http://77.244.215.49:8000/"

function Square(props) {
	return (
		<button className="square" onClick={() => props.onClick()}>
			{props.value}
		</button>
	);
}
class Board extends React.Component {
	constructor() {
		super(...arguments);
		this.state = {
			squares: new Array(9),
			xIsNext: true,
			inDate: new Array(9)
		};
		componentWillMount() {
		 fetch(API_URL)
				 .then((response) => response.json())
					 .then((responseData) => {
							 this.setState({inDate: Object.get.OwnPropertyNames(responseData).sort()});
					 })
				 .catch((error) => {
						 console.log('Error fetching and parsing data', error);
					 });
	}
	componentDidMount() {
		for (let i = 0; i < 10; i++) {
			if(inDate[i] == '1'){
				squares[i] = 'X'
			}
			if(inDate[i] == '-1'){
				squares[i] = 'O'
			}
			this.state.xIsNext ? 'X' : 'O';
		}
	}

	renderSquare(i) {
		return <Square value={this.state.squares[i]} onClick={() => this.handleClick(i)} />;
	}
	handleClick(i) {
		const squares = this.state.squares.slice();
		if ( squares[i] || calculateWinner(squares) ) {
			return;
		}
		squares[i] = this.state.xIsNext ? 'X' : 'O';
		this.setState({
			squares: squares,
			xIsNext: !this.state.xIsNext
		});
	}
	render() {
		const winner = calculateWinner(this.state.squares);
		let status;
		if (winner) {
			status = 'Winner: ' + winner;
		} else {
			status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
		}
		return (
			<div>
				<div className="status">{status}</div>
				<div className="board-row">
					{this.renderSquare(0)}
					{this.renderSquare(1)}
					{this.renderSquare(2)}
				</div>
				<div className="board-row">
					{this.renderSquare(3)}
					{this.renderSquare(4)}
					{this.renderSquare(5)}
				</div>
				<div className="board-row">
					{this.renderSquare(6)}
					{this.renderSquare(7)}
					{this.renderSquare(8)}
				</div>
			</div>
		);
	}
}
ReactDOM.render(
	<Board />,
	document.getElementById('container')
);
function calculateWinner(squares) {
	const lines = [
		[0, 1, 2],
		[3, 4, 5],
		[6, 7, 8],
		[0, 3, 6],
		[1, 4, 7],
		[2, 5, 8],
		[0, 4, 8],
		[2, 4, 6],
	];
	const length = lines.length;
	for (let i = 0; i < length; i++) {
		const [a, b, c] = lines[i];
		const player = squares[a];
		if (player && player === squares[b] && player === squares[c]) {
			return player;
		}
	}
	return null;
}
